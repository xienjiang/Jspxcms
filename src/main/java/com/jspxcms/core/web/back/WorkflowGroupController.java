package com.jspxcms.core.web.back;

import com.jspxcms.common.orm.RowSide;
import com.jspxcms.common.web.PathResolver;
import com.jspxcms.common.web.Servlets;
import com.jspxcms.core.constant.Constants;
import com.jspxcms.core.domain.Site;
import com.jspxcms.core.domain.WorkflowGroup;
import com.jspxcms.core.service.*;
import com.jspxcms.core.support.Backends;
import com.jspxcms.core.support.Context;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

import static com.jspxcms.core.constant.Constants.*;

/**
 * @author DELL
 */
@Controller
@RequestMapping("/core/workflow_group")
public class WorkflowGroupController {
    private static final Logger logger = LoggerFactory.getLogger(WorkflowGroupController.class);

    @Autowired
    private WorkflowGroupService workflowGroupService;

    @Autowired
    private OperationLogService logService;
    @Autowired
    private PublishPointService publishPointService;
    @Autowired
    private OrgService orgService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private NodeQueryService nodeQuery;
    @Autowired
    private SitePortService sitePortService;
    @Autowired
    private SiteService service;
    @Autowired
    private PathResolver pathResolver;

    @RequestMapping("list.do")
    @RequiresRoles("super")
    @RequiresPermissions("core:workflow_group:list")
    public String list(
            @PageableDefault(sort = {"seq", "id"}, direction = Sort.Direction.ASC) Pageable pageable, HttpServletRequest request,
            org.springframework.ui.Model modelMap) {
        Map<String, String[]> params = Servlets.getParamValuesMap(request, Constants.SEARCH_PREFIX);
        Integer siteId = Context.getCurrentSiteId();
        List<WorkflowGroup> list = workflowGroupService.findList(siteId, params, pageable.getSort());
        modelMap.addAttribute("list", list);
        return "core/workflow_group/workflow_group_list";
    }

    @RequestMapping("create.do")
    @RequiresRoles("super")
    @RequiresPermissions("core:workflow_group:create")
    public String create(Integer id, HttpServletRequest request, org.springframework.ui.Model modelMap) {
        Site site = Context.getCurrentSite();
        if (id != null) {
            WorkflowGroup bean = workflowGroupService.get(id);
            Backends.validateDataInSite(bean, site.getId());
            modelMap.addAttribute("bean", bean);
        }
        modelMap.addAttribute(OPRT, CREATE);
        return "core/workflow_group/workflow_group_form";
    }

    @RequestMapping("edit.do")
    @RequiresRoles("super")
    @RequiresPermissions("core:workflow_group:edit")
    public String edit(Integer id, Integer position,
                       @PageableDefault(sort = {"seq", "id"}, direction = Sort.Direction.ASC) Pageable pageable,
                       HttpServletRequest request, org.springframework.ui.Model modelMap) {
        Integer siteId = Context.getCurrentSiteId();
        WorkflowGroup bean = workflowGroupService.get(id);
        Backends.validateDataInSite(bean, siteId);
        Map<String, String[]> params = Servlets.getParamValuesMap(request,
                Constants.SEARCH_PREFIX);
        RowSide<WorkflowGroup> side = workflowGroupService.findSide(siteId, params, bean, position,
                pageable.getSort());
//        modelMap.addAttribute("infoPerms", bean.getInfoPerms());
//        modelMap.addAttribute("nodePerms", bean.getNodePerms());
        modelMap.addAttribute("bean", bean);
        modelMap.addAttribute("side", side);
        modelMap.addAttribute("position", position);
        modelMap.addAttribute(OPRT, EDIT);
        return "core/workflow_group/workflow_group_form";
    }

//    @RequestMapping("export.do")
//    @RequiresRoles("super")
////    @RequiresPermissions("core:workflow_group:exportNode")
//    public void exportSite(Integer id, HttpServletResponse response) throws JAXBException, IOException {
//        Site site = service.get(id);
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("text/html;charset=utf-8");
//        File dir = sitePortService.exportAll(site);
////        List<File> list = new ArrayList<>();
////        Collections.addAll(list, dir.listFiles());
////        LocalFileHandler fileHandler = FileHandler.getLocalFileHandler(pathResolver, "");
////        list.add(fileHandler.getFile(site.getSiteBase("")));
//        response.setContentType("application/x-download;charset=UTF-8");
//        response.addHeader("Content-disposition", "filename=site_" + site.getId() + ".zip");
//        try {
//            AntZipUtils.zip(dir.listFiles(), response.getOutputStream());
//            FileUtils.deleteQuietly(dir);
//        } catch (IOException e) {
//            logger.error("zip error!", e);
//        }
//    }
//
//    @RequestMapping("import.do")
//    public String importSite(@RequestParam(value = "file", required = false) MultipartFile file,
//                             HttpServletRequest request, HttpServletResponse response, RedirectAttributes ra)
//            throws IOException, JAXBException {
//        User user = Context.getCurrentUser();
//        File tempFile = FilesEx.getTempFile();
//        file.transferTo(tempFile);
//        File unzipFile = FilesEx.getTempFile();
//        AntZipUtils.unzip(tempFile, unzipFile);
//        FileUtils.deleteQuietly(tempFile);
//        sitePortService.importAll(unzipFile, user.getId());
//        FileUtils.deleteQuietly(unzipFile);
//        ra.addFlashAttribute(MESSAGE, OPERATION_SUCCESS);
//        return "redirect:list.do";
//    }

    @RequestMapping("save.do")
    @RequiresRoles("super")
    @RequiresPermissions("core:workflow_group:save")
    public String save(WorkflowGroup bean,
                       String redirect, HttpServletRequest request, RedirectAttributes ra) {

        Integer siteId = Context.getCurrentSiteId();
        workflowGroupService.save(bean, siteId);


//        Integer userId = Context.getCurrentUserId();
        logService.operation("opr.workflow_group.add", bean.getName(), null, bean.getId(), request);
        logger.info("save workflow_group, name={}.", bean.getName());
        ra.addFlashAttribute(MESSAGE, SAVE_SUCCESS);
        if (Constants.REDIRECT_LIST.equals(redirect)) {
            return "redirect:list.do";
        } else if (Constants.REDIRECT_CREATE.equals(redirect)) {
            return "redirect:create.do";
        } else {
            ra.addAttribute("id", bean.getId());
            return "redirect:edit.do";
        }
    }

    @RequestMapping("update.do")
    @RequiresRoles("super")
    @RequiresPermissions("core:workflow_group:update")
    public String update(@ModelAttribute("bean") WorkflowGroup bean, Integer parentId,
                         Integer orgId, Integer htmlPublishPointId, Integer mobilePublishPointId, Integer position,
                         String redirect, HttpServletRequest request, RedirectAttributes ra) {
//        Integer siteId = Context.getCurrentSiteId();
        workflowGroupService.update(bean);

        logService.operation("opr.workflow_group.edit", bean.getName(), null, bean.getId(), request);
        logger.info("update workflow_group, name={}.", bean.getName());
        ra.addFlashAttribute(MESSAGE, SAVE_SUCCESS);
        if (Constants.REDIRECT_LIST.equals(redirect)) {
            return "redirect:list.do";
        } else {
            ra.addAttribute("id", bean.getId());
            ra.addAttribute("position", position);
            return "redirect:edit.do";
        }
    }

    @RequestMapping("delete.do")
    @RequiresRoles("super")
    @RequiresPermissions("core:workflow_group:delete")
    public String delete(Integer[] ids, HttpServletRequest request, RedirectAttributes ra) {

        WorkflowGroup[] beans = workflowGroupService.delete(ids);
        for (WorkflowGroup bean : beans) {
            logService.operation("opr.workflow_group.delete", bean.getName(), null, bean.getId(), request);
            logger.info("delete workflow_group, name={}.", bean.getName());
        }
        ra.addFlashAttribute(MESSAGE, DELETE_SUCCESS);
        return "redirect:list.do";
    }


    @ModelAttribute("bean")
    public WorkflowGroup preloadBean(@RequestParam(required = false) Integer oid) {
        return oid != null ? workflowGroupService.get(oid) : null;
    }
}
