package com.jspxcms.core.service.impl;

import com.jspxcms.common.ip.IPSeeker;
import com.jspxcms.common.orm.Limitable;
import com.jspxcms.common.orm.RowSide;
import com.jspxcms.common.orm.SearchFilter;
import com.jspxcms.core.domain.Comment;
import com.jspxcms.core.domain.Site;
import com.jspxcms.core.domain.User;
import com.jspxcms.core.domain.UserCustomNode;
import com.jspxcms.core.listener.SiteDeleteListener;
import com.jspxcms.core.listener.UserDeleteListener;
import com.jspxcms.core.repository.CommentDao;
import com.jspxcms.core.repository.UserCustomNodeDao;
import com.jspxcms.core.service.CommentService;
import com.jspxcms.core.service.SiteService;
import com.jspxcms.core.service.UserCustomNodeService;
import com.jspxcms.core.service.UserService;
import com.jspxcms.core.support.Commentable;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.*;

/**
 * CommentServiceImpl
 * 
 * @author liufang
 *
 */
@Service
@Transactional
//@Transactional(readOnly = true)
public class UserCustomNodeServiceImpl implements UserCustomNodeService{//, SiteDeleteListener, UserDeleteListener {


	private UserCustomNodeDao dao;

	@Autowired
	public void setDao(UserCustomNodeDao dao) {
		this.dao = dao;
	}

	@Override
	public UserCustomNode findOne(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public UserCustomNode save(UserCustomNode bean) {
		return dao.save(bean);
	}

	@Override
	public int saveList(List<UserCustomNode> list) {
//		Session session = sessionFactory.openSession();
//		Transaction tx = session.beginTransaction();

		for ( int i=0; i<list.size(); i++ ) {
			dao.save(list.get(i));
//			Customer customer = new Customer(.....);
//			session.save(list.get(i));
//			if ( i % 20 == 0 ) { //单次批量操作的数目为20
//				session.flush(); //清理缓存，执行批量插入20条记录的SQL insert语句
//				session.clear(); //清空缓存中的Customer对象
//			}
		}

//		tx.commit();
//		session.close();
		return 0;
	}

	@Override
	public void delete(UserCustomNode bean) {
		dao.delete(bean);
	}

	@Override
	public List<UserCustomNode> findByTypeAndUserId(Integer type, Integer userId) {
		return dao.findByTypeAndUserId(type,userId);
	}

	@Override
	public int deleteByTypeAndUserId(Integer type, Integer userId) {
		return dao.deleteByTypeAndUserId(type,userId);
	}
}
