package com.jspxcms.core.service;

import com.jspxcms.common.orm.Limitable;
import com.jspxcms.common.orm.RowSide;
import com.jspxcms.core.domain.Comment;
import com.jspxcms.core.domain.UserCustomNode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * CommentService
 * 
 * @author liufang
 * 
 */
public interface UserCustomNodeService {
	UserCustomNode findOne(Integer id);

	UserCustomNode save(UserCustomNode bean);
	int saveList(List<UserCustomNode> list);

	void delete(UserCustomNode bean);


	List<UserCustomNode> findByTypeAndUserId(Integer type, Integer userId);

	int deleteByTypeAndUserId(Integer type, Integer userId);
}
