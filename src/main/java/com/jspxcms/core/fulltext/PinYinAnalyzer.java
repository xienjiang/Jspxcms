package com.jspxcms.core.fulltext;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.wltea.analyzer.lucene.IKTokenizer;

import java.io.Reader;

public class PinYinAnalyzer extends Analyzer{


	@Override
	public TokenStream tokenStream(String s, Reader reader) {
		IKTokenizer tokenizer=new IKTokenizer(reader, false);
		PinYinTokenFilter filter=new PinYinTokenFilter(tokenizer);
		PinyinNGramTokenFilter filter2=new PinyinNGramTokenFilter(filter, 8, 10);
		return filter2;
	}
}
