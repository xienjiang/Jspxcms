package com.jspxcms.core.fulltext;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

import java.io.IOException;
import java.io.StringReader;

public class Demo {
    public static void main(String[] args) throws IOException {
        String str="中国";
        Analyzer analyzer=new PinYinAnalyzer();
        TokenStream stream = analyzer.tokenStream("content", new StringReader(str));
        PositionIncrementAttribute pia = stream.addAttribute(PositionIncrementAttribute.class);
        OffsetAttribute oa = stream.addAttribute(OffsetAttribute.class);
        CharTermAttribute cta = stream.addAttribute(CharTermAttribute.class);
        TypeAttribute ta = stream.addAttribute(TypeAttribute.class);
        while (stream.incrementToken()) {
            System.out.print("位置增量： " + pia.getPositionIncrement());//词与词之间的空格
            System.out.print("，单词： " + cta + "[" + oa.startOffset() + "," + oa.endOffset() + "]");
            System.out.print("，类型： " + ta.type()) ; System.out.println(); }

    }
}
