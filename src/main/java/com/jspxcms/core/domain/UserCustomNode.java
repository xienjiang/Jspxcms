package com.jspxcms.core.domain;

import com.jspxcms.common.web.Anchor;
import com.jspxcms.core.support.Siteable;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Comment评论对象
 * 
 * @author liufang
 * 
 */
@Entity
@Table(name = "user_cust_node_index")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class UserCustomNode {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="node_id")
	private int nodeId;

	@Column(name="sort_index")
	private int index;

	@Column(name="user_id")
	private int userId;

	@Column(name="type")
	private int type;


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
