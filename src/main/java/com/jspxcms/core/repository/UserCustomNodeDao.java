package com.jspxcms.core.repository;


import com.jspxcms.core.domain.UserCustomNode;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/*
  *用户首页个人配置
 */
public interface UserCustomNodeDao extends Repository<UserCustomNode, Integer> {

    UserCustomNode findOne(Integer id);

    UserCustomNode save(UserCustomNode bean);

    void delete(UserCustomNode bean);

    // --------------------

    /**
     * 查询评论数量
     *
     * @param siteId    站点ID
     * @param beginDate 起始时间
     * @return 数据数量
     */
    @Query("from UserCustomNode bean where bean.type=?1 and bean.userId=?2")
    List<UserCustomNode> findByTypeAndUserId(Integer type, Integer userId);


    @Modifying
    @Query("delete from UserCustomNode bean where bean.type=?1 and bean.userId=?2")
    int deleteByTypeAndUserId(Integer type, Integer userId);
}
