/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50633
 Source Host           : localhost:3306
 Source Schema         : jcms

 Target Server Type    : MySQL
 Target Server Version : 50633
 File Encoding         : 65001

 Date: 13/08/2018 00:53:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_cust_node_index
-- ----------------------------
DROP TABLE IF EXISTS `user_cust_node_index`;
CREATE TABLE `user_cust_node_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表ID',
  `node_id` int(255) DEFAULT NULL COMMENT '栏目ID',
  `sort_index` int(255) DEFAULT NULL COMMENT '顺序',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `type` int(11) DEFAULT NULL COMMENT '0：导航，1:新闻 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=400 DEFAULT CHARSET=utf8 COMMENT='用户自定义栏目顺序';

SET FOREIGN_KEY_CHECKS = 1;
